
package com.fhs.platform.common.constants;

import com.fhs.platform.common.utils.MessageUtils;
import java.util.Arrays;
import org.springframework.http.HttpStatus;

public enum ConstraintConstants {

    NOT_BLANK("NotBlank", CommonMessages.REQUIRED, new SimpleMessageProvider()),
    NOT_NULL("NotNull", CommonMessages.REQUIRED, new SimpleMessageProvider()),
    NOT_EMPTY("NotEmpty", CommonMessages.REQUIRED, new SimpleMessageProvider()),
    SIZE("Size", CommonMessages.SIZE_BETWEEN, new SizeBetweenMessageProvider()),
    LENGTH("Length", CommonMessages.MAXLENGTH, new StringLengthMessageProvider()),
    EMAIL("Email", CommonMessages.EMAIL, new SimpleMessageProvider()),
    EMAIL_LIST("EmailList", CommonMessages.EMAIL, new SimpleMessageProvider()),
    MIN("Min", CommonMessages.NUMBER_MIN, new SingleParameterMessageProvider()),
    MAX("Max", CommonMessages.NUMBER_MAX, new SingleParameterMessageProvider()),
    PAST("Past", CommonMessages.PAST, new SimpleMessageProvider()),
    PATTERN("Pattern", CommonMessages.PATTERN, new SimpleMessageProvider()),
    FUTURE("Future", CommonMessages.FUTURE, new SimpleMessageProvider()),
    DATE("Date", CommonMessages.INVALID, new SimpleMessageProvider()),
    DATE_TIME("DateTime", CommonMessages.INVALID, new SimpleMessageProvider()),
    ZONE_DATE_TIME("ZoneDateTime", CommonMessages.INVALID, new SimpleMessageProvider()),
    MUTUALLY_EXCLUSIVE("MutuallyExclusive", CommonMessages.FIELDS_MUTUALLY_EXCLUSIVE, new MutuallyExclusiveMessageProvider()),
    PERIOD("Period", CommonMessages.INVALID_PERIOD, new SimpleMessageProvider()),
    DEFAULT(HttpStatus.BAD_REQUEST.name(), CommonMessages.INVALID, new DefaultMessageProvider());

    private final String errorCode;
    private final String messageCode;
    private final ConstraintMessageProvider provider;

    ConstraintConstants(String errorCode, String messageCode, ConstraintMessageProvider provider) {
        this.errorCode = errorCode;
        this.messageCode = messageCode;
        this.provider = provider;
    }

    public static ConstraintConstants valueOfErrorCode(String errorCode) {
        return Arrays.stream(values())
                .filter(c -> c.errorCode().equals(errorCode))
                .findFirst()
                .orElse(DEFAULT);
    }

    public String errorCode() {
        return errorCode;
    }

    public String messageCode() {
        return messageCode;
    }

    public String message(MessageUtils messageUtil, Object rejectedValue, String fieldName, Object... params) {
        return provider.getMessage(messageUtil, messageCode(), rejectedValue, fieldName, params);
    }



    interface ConstraintMessageProvider {

        String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params);

    }

    static class SimpleMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            return messageUtil.getMessage(code, fieldName);
        }

    }

    static class DefaultMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            try {
                return messageUtil.getMessage(code, params);
            } catch (Exception ex) {
                return messageUtil.getMessage(CommonMessages.INVALID, params);
            }
        }

    }

    static class SingleParameterMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            Object param = params[params.length - 1];
            return messageUtil.getMessage(code, fieldName, param);
        }

    }

    static class SizeBetweenMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {

            Object min = params[params.length - 1];
            Object max = params[params.length - 2];
            return messageUtil.getMessage(code, fieldName, min, max);
        }

    }

    static class RejectedValueProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            return messageUtil.getMessage(code, rejectedValue);
        }

    }

    static class StringLengthMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            Integer min = (Integer) params[params.length - 1];
            Integer max = (Integer) params[params.length - 2];

            if (min == 0 && max != Integer.MAX_VALUE) {
                return messageUtil.getMessage(CommonMessages.MAXLENGTH, fieldName, max);
            } else if (min != 0 && max == Integer.MAX_VALUE) {
                return messageUtil.getMessage(CommonMessages.MINLENGTH, fieldName, min);
            }

            return messageUtil.getMessage(CommonMessages.SIZE_BETWEEN, fieldName, min, max);
        }

    }

    static class MutuallyExclusiveMessageProvider implements ConstraintMessageProvider {

        @Override
        public String getMessage(MessageUtils messageUtil, String code, Object rejectedValue, String fieldName, Object... params) {
            String field1 = String.valueOf(params[params.length - 1]);
            String field2 = String.valueOf(params[params.length - 2]);
            return messageUtil.getMessage(CommonMessages.FIELDS_MUTUALLY_EXCLUSIVE, field1, field2);
        }

    }
}

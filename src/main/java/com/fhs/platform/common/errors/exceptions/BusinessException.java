package com.fhs.platform.common.errors.exceptions;

import com.fhs.platform.common.constants.CommonMessages;
import com.fhs.platform.common.utils.MessageUtils;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * TODO: Add support for multiple errors
 */
@Getter
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -6120938435636634793L;

    private HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    private String code = CommonMessages.HTTP_BAD_REQUEST;

    @SuppressWarnings("all")
    private Object[] args;

    public BusinessException(String code, Object... args) {
        this.code = code;
        this.args = args;
    }

    public BusinessException(HttpStatus httpStatus, String code, Object... args) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.args = args;
    }

    public BusinessException() {
    }

    public BusinessException(String code) {
        super();
        this.code = code;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(
            String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ErrorMessage getErrorMessage(String prefix , MessageUtils messageUtils) {
        ErrorMessage errorMessage = new ErrorMessage();
        String message = messageUtils.getMessage(code, args);
        ErrorDetail errorDetail = new ErrorDetail();
        errorDetail.setDetails(message);
        errorDetail.setCode(prefix.concat(code));
        errorMessage.addErrorDetail(errorDetail);
        return errorMessage;
    }

}

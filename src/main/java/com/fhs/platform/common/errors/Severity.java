
package com.fhs.platform.common.errors;

public enum Severity {
    FATAL("fatal"),
    ERROR("error"),
    WARNING("warning"),
    INFORMATION("information");

    private final String message;

    Severity(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}

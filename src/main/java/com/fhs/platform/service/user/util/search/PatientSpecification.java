package com.fhs.platform.service.user.util.search;


import com.fhs.platform.service.user.constants.gender.Gender;
import com.fhs.platform.service.user.domain.PatientFilter;
import com.fhs.platform.service.user.entity.Patient;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
public class PatientSpecification implements Specification<Patient> {

    private static final long serialVersionUID = -8930096172413943612L;
    private final PatientFilter patientFilter;

    @Override
    public Predicate toPredicate
            (Root<Patient> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("isDeleted"), false));
        if (StringUtils.isNotBlank(patientFilter.getGender())) {
            predicates.add(criteriaBuilder.equal(root.get("gender"), Gender.valueOf(patientFilter.getGender())));
        }
        if (StringUtils.isNotBlank(patientFilter.getName())) {
            Expression<String> familyNameConcatGivenName = criteriaBuilder.concat(root.get("familyName"), " ");
            familyNameConcatGivenName = criteriaBuilder.concat(familyNameConcatGivenName, root.get("givenName"));
            Expression<String> givenNameConcatFamilyName = criteriaBuilder.concat(root.<String>get("givenName"), " ");
            givenNameConcatFamilyName = criteriaBuilder.concat(givenNameConcatFamilyName, root.get("familyName"));
            Predicate whereClause = criteriaBuilder.or(criteriaBuilder.like(

                    criteriaBuilder.lower(familyNameConcatGivenName), "%" + patientFilter.getName().toLowerCase() + "%"), criteriaBuilder.like(

                    criteriaBuilder.lower(givenNameConcatFamilyName), "%" + patientFilter.getName().toLowerCase() + "%"));
            predicates.add(whereClause);
        }

        if (StringUtils.isNotBlank(patientFilter.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"), UUID.fromString(patientFilter.getId())));
        }
        if (StringUtils.isNotBlank(patientFilter.getStartBirthDate())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("birthDate"), patientFilter.getStartBirthDate()));
        }
        if (StringUtils.isNotBlank(patientFilter.getEndBirthDate())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("birthDate"), patientFilter.getEndBirthDate()));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }


}
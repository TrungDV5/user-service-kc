package com.fhs.platform.service.user.integration.dto;

import com.fhs.platform.service.user.constants.KeycloakIntegrationConstants;
import com.fhs.platform.service.user.entity.Patient;
import lombok.Getter;
import lombok.Setter;
import java.util.UUID;

@Getter
@Setter
public class KeycloakUserDto {

    private UUID userId;
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String clientRoleName;

    public static KeycloakUserDto patientToKeycloakUserDto(Patient patient) {
        KeycloakUserDto keycloakUserDto = new KeycloakUserDto();
        keycloakUserDto.setUsername(patient.getEmail());
        keycloakUserDto.setEmail(patient.getEmail());
        keycloakUserDto.setFirstName(patient.getFamilyName());
        keycloakUserDto.setLastName(patient.getGivenName());
        keycloakUserDto.setPassword(KeycloakIntegrationConstants.DEFAULT_PASSWORD);
        keycloakUserDto.setClientRoleName(patient.getRole().getCode().toLowerCase());
        return keycloakUserDto;
    }

    ;
}

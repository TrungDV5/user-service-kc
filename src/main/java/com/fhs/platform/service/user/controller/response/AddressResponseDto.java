package com.fhs.platform.service.user.controller.response;

import java.util.UUID;
import lombok.Data;

@Data
public class AddressResponseDto {

    private UUID id;
    private String use;
    private String type;
    private String text;
    private String line;
    private String city;
    private String district;
    private String state;
    private String postalCode;
    private String country;

}

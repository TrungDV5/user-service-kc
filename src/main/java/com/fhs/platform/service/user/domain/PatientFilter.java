package com.fhs.platform.service.user.domain;

import lombok.Data;

@Data
public class PatientFilter {

    private String id;
    private String gender;
    private String name;
    private String startBirthDate;
    private String endBirthDate;

}

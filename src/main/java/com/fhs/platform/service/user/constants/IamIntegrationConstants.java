package com.fhs.platform.service.user.constants;

public class IamIntegrationConstants {

    public static final String USER_ROLE_NAME = "USER";
    public static final String PATIENT_ROLE_NAME = "PATIENT";
    public static final String PRACTITIONER_ROLE_NAME = "PRACTITIONER";
    public static final String USER_AUTHORITY = "user";
    public static final String DEFAULT_PASSWORD = "pass";
    public static final String DEFAULT_BASE_URL = "http://localhost:8080";

}

package com.fhs.platform.service.user.integration;

import com.fhs.platform.service.user.constants.KeycloakIntegrationConstants;
import com.fhs.platform.service.user.integration.config.KeycloakProperties;
import com.fhs.platform.service.user.integration.dto.KeycloakUserDto;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

@Component
public class KeycloakServiceIntegration {

    @Autowired
    private KeycloakProperties keycloakProperties;

    @PostConstruct
    private void setKeycloakProperties() {
        this.serverUrl = keycloakProperties.getAuthServerUrl();
        this.realm = keycloakProperties.getRealm();
        this.clientId = keycloakProperties.getClientId();
        this.clientSecret = keycloakProperties.getClientSecret();
        this.adminPassword = keycloakProperties.getAdminPassword();
        this.adminUsername = keycloakProperties.getAdminUsername();
    }

    private String serverUrl = "https://keycloak.khambenh247.com/auth";
    private String realm = "akahealth";
    // idm-client needs to allow "Direct Access Grants: Resource Owner Password Credentials Grant"
    private String clientId = "user-service";
    private String clientSecret = "4a621a73-4a07-4609-8040-5cacf06ae483";
    private String adminUsername = "employee2";
    private String adminPassword = "user";
    private static Keycloak keycloak;

    public Keycloak getKeyCloak() {
        if (keycloak == null || keycloak.isClosed()) {
            // User "idm-admin" needs at least "manage-users, view-clients, view-realm, view-users" roles for "realm-management"
            return KeycloakBuilder.builder()
                    .serverUrl(serverUrl)
                    .realm(realm)
                    .grantType(OAuth2Constants.PASSWORD)
                    .clientId(clientId)
                    .clientSecret(clientSecret)
                    .username(adminUsername)
                    .password(adminPassword)
                    .build();
        } else {
            return keycloak;
        }
    }

    public void createUser(KeycloakUserDto keycloakUserDto) {
        Keycloak keycloak = getKeyCloak();
        // Define user
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(keycloakUserDto.getUsername());
        user.setFirstName(keycloakUserDto.getFirstName());
        user.setLastName(keycloakUserDto.getLastName());
        user.setEmail(keycloakUserDto.getEmail());
        // TODO need to verify these attributes
        user.setAttributes(Collections.singletonMap("origin", Arrays.asList("demo")));

        // Get realm
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersRessource = realmResource.users();

        // Create user (requires manage-users role)
        Response response = usersRessource.create(user);
        System.out.printf("Repsonse: %s %s%n", response.getStatus(), response.getStatusInfo());
        System.out.println(response.getLocation());
        String userId = CreatedResponseUtil.getCreatedId(response);

        System.out.printf("User created with userId: %s%n", userId);

        UserResource userResource = usersRessource.get(userId);

        setUserPassword(userResource, keycloakUserDto.getPassword(), false);

        assignRolesToUser(realmResource, userResource, keycloakUserDto.getClientRoleName());

        keycloakUserDto.setUserId(UUID.fromString(userId));
    }

    private void assignRolesToUser(RealmResource realmResource, UserResource userResource, String clientRole) {
        assignRealmRoleToUser(realmResource, userResource);
        assignClientRoleToUser(realmResource, userResource, clientRole);
    }

    private void assignClientRoleToUser(RealmResource realmResource, UserResource userResource, String clientRole) {
        // Get client
        ClientRepresentation appClient = realmResource.clients()
                .findByClientId(clientId).get(0);
        // Get client level role (requires view-clients role)
        RoleRepresentation userClientRole = realmResource.clients().get(appClient.getId())
                .roles().get(clientRole).toRepresentation();
        // Assign client level role to user
        userResource.roles()
                .clientLevel(appClient.getId()).add(Arrays.asList(userClientRole));
    }

    private void assignRealmRoleToUser(RealmResource realmResource, UserResource userResource) {
        // Get realm role "app-user" (requires view-realm role)
        RoleRepresentation testerRealmRole = realmResource.roles()
                .get(KeycloakIntegrationConstants.APP_USER_REALM_ROLE_NAME).toRepresentation();

        // Assign realm role app-user to user
        userResource.roles().realmLevel()
                .add(Arrays.asList(testerRealmRole));
    }

    private void setUserPassword(UserResource userResource, String userPassword, boolean isTemporary) {
        // Define password credential
        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(isTemporary);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(userPassword);
        // Set password credential
        userResource.resetPassword(passwordCred);
    }

}

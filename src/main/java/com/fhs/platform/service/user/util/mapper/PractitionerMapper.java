package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.PractitionerCreateDto;
import com.fhs.platform.service.user.controller.request.update.PractitionerUpdateDto;
import com.fhs.platform.service.user.controller.response.PractitionerResponseDto;
import com.fhs.platform.service.user.entity.Practitioner;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PractitionerMapper {

    Practitioner practitionerDtoToPractitioner(PractitionerCreateDto practitionerDto);
    @Mapping(target = "birthDate", dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX")
    PractitionerResponseDto practitionerToPractitionerResponseDto(Practitioner practitioner);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePatientFromDto(PractitionerUpdateDto practitionerUpdateDto, @MappingTarget Practitioner practitioner);

}

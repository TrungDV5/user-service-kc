package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import com.fhs.platform.service.user.constants.addresstype.AddressType;
import com.fhs.platform.service.user.constants.addressuse.AddressUse;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@NoArgsConstructor
public class Address extends BaseEntity {

    private static final long serialVersionUID = -8928241805421078872L;
    private AddressUse use;
    private AddressType type;
    private String text;
    private String line;
    private String city;
    private String district;
    private String state;
    private String postalCode;
    private String country;

    @OneToOne(mappedBy = "address")
    @JsonIgnore
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "practitioner_id")
    @JsonIgnore
    private Practitioner practitioner;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    private Patient patient;

}

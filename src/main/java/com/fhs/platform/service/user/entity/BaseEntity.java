package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@MappedSuperclass
/*BaseEntity*/

public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = -5507740661014003787L;

    @Id
    @GenericGenerator(name = "UseExistingIdOtherwiseAutoGenerate",
                      strategy = "com.fhs.platform.service.user.util.generator.UseExistingIdOtherwiseAutoGenerate")
    @GeneratedValue(generator = "UseExistingIdOtherwiseAutoGenerate")
    @Column(unique = true, nullable = false)
    private UUID id;

    @NotNull
    @Column(name = "created_by")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private UUID createdBy;

    @NotNull
    @Column(name = "created_on")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private Timestamp createdOn;

    @NotNull
    @Column(name = "updated_by")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private UUID updatedBy;

    @NotNull
    @Column(name = "updated_on")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private Timestamp updatedOn;

    @NotNull
    @Column(name = "is_deleted")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private Boolean isDeleted = false;


    @PrePersist
    protected void preparePersist() {
        //hardcoded
        createdBy = UUID.randomUUID();
        updatedBy = UUID.randomUUID();
        createdOn = Timestamp.from(Instant.now());
        updatedOn = Timestamp.from(Instant.now());
    }

    @PreUpdate
    protected void prepareUpdate() {
        //hardcoded
        updatedBy = UUID.randomUUID();
        updatedOn = Timestamp.from(Instant.now());
    }

}

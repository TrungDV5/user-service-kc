package com.fhs.platform.service.user.domain;

import lombok.Data;

@Data
public class PractitionerFilter {

    private String id;
    private String gender;

    private String name;
    private String startBirthDate;
    private String endBirthDate;
    private String minimumExperience;
    private String maximumExperience;

}

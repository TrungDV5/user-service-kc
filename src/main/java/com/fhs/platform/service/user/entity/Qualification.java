package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class Qualification extends BaseEntity {

    private String code;
    private String name;
    private ZonedDateTime startValid;
    private ZonedDateTime endValid;
    private String issuer;

    @ManyToOne
    @JoinColumn(name = "practitioner_id")
    @JsonIgnore
    private Practitioner practitioner;

}

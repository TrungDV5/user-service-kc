package com.fhs.platform.service.user.constants.maritalstatus;


import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MaritalStatusConverter implements AttributeConverter<MaritalStatus, String> {

    @Override
    public String convertToDatabaseColumn(MaritalStatus maritalStatus) {
        if (maritalStatus == null) {
            return "UNK";
        }
        return maritalStatus.getCode();
    }

    @Override
    public MaritalStatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(MaritalStatus.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}

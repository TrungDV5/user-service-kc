package com.fhs.platform.service.user.constants.specialty;

import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)

public class SpecialtyConverter implements AttributeConverter<Specialty, String> {

    @Override
    public String convertToDatabaseColumn(Specialty specialty) {
        if (specialty == null) {
            return null;
        }
        return specialty.getCode();
    }

    @Override
    public Specialty convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(Specialty.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}

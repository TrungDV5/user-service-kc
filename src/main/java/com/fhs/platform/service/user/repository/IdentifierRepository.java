package com.fhs.platform.service.user.repository;

import com.fhs.platform.service.user.constants.identifiertype.IdentifierType;
import com.fhs.platform.service.user.entity.Identifier;
import com.fhs.platform.service.user.entity.Patient;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentifierRepository extends JpaRepository<Identifier, UUID> {

    Collection<Patient> findByIsDeleted(boolean isDeleted);

    boolean existsByIdentifierTypeAndValueAndIsDeleted(IdentifierType identifierType, String value, boolean isDeleted);

    Optional<Identifier> findByIdentifierTypeAndValue(IdentifierType identifierType, String value);

}

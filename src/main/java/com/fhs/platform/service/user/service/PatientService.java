package com.fhs.platform.service.user.service;

import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.ContactCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PatientCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.ContactUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PatientUpdateDto;
import com.fhs.platform.service.user.controller.response.*;
import com.fhs.platform.service.user.domain.PatientFilter;
import java.util.List;
import java.util.UUID;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface PatientService {

    PatientResponseDto getPatientById(UUID patientId);

    PatientResponseDto createPatient(PatientCreateDto patientCreateDTO);

    PatientResponseDto updatePatientById(UUID patientId, PatientUpdateDto patientUpdateDto);

    DeleteResponse deletePatientById(UUID patientId);

   PagingResponseDTO<?> getPatients(Pageable pageable, PatientFilter patientFilter);


    List<AddressResponseDto> getPatientAddresses(UUID patientId);

    AddressResponseDto createPatientAddress(UUID patientId, AddressCreateDto addressDto);

    AddressResponseDto updatePatientAddress(UUID patientId, UUID addressId, AddressUpdateDto addressUpdateDto);

    DeleteResponse deletePatientAddressById(UUID patientId, UUID addressId);

    AddressResponseDto getPatientAddressById(UUID patientId, UUID addressId);

    List<IdentifierResponseDto> getPatientIdentifiers(UUID patientId);

    IdentifierResponseDto createPatientIdentifier(UUID patientId, IdentifierCreateDto identifierDto);

    IdentifierResponseDto updatePatientIdentifier(UUID patientId, UUID identifierId, IdentifierUpdateDto identifierUpdateDto);

    DeleteResponse deletePatientIdentifier(UUID patientId, UUID identifierId);

    IdentifierResponseDto getPatientIdentifierById(UUID patientId, UUID identifierId);

    List<ContactResponseDto> getPatientContacts(UUID patientId);

    ContactResponseDto createPatientContact(UUID patientId, ContactCreateDto contactDto);

    ContactResponseDto updatePatientContact(UUID patientId, UUID contactId, ContactUpdateDto contactUpdateDto);

    DeleteResponse deletePatientContact(UUID patientId, UUID contactId);

    ContactResponseDto getPatientContactById(UUID patientId, UUID contactId);

    AttachmentResponseDto uploadAvatarToPatient(MultipartFile avatar, UUID patientId);

    Resource loadAvatarFile(String filename, UUID patientId);

}

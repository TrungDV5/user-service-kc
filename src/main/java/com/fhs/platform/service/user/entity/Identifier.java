package com.fhs.platform.service.user.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import com.fhs.platform.service.user.constants.identifiertype.IdentifierType;
import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"identifierType", "value"})
})
@NoArgsConstructor
public class Identifier extends BaseEntity {

    private IdentifierType identifierType;
    private String value;
    private ZonedDateTime startValid;
    private ZonedDateTime endValid;
    private String assigner;

    @ManyToOne
    @JoinColumn(name = "practitioner_id")
    @JsonIgnore
    private Practitioner practitioner;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    private Patient patient;

}

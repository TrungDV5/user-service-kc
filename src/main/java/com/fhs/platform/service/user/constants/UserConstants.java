package com.fhs.platform.service.user.constants;

public class UserConstants {

    public static final String PATIENT_NOT_FOUND_MESSAGE_EXCEPTION = "Patient not found";
    public static final String ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION = "Address not found";
    public static final String IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION = "Identifier not found";
    public static final String CONTACT_NOT_FOUND_MESSAGE_EXCEPTION = "Contact not found";
    public static final String QUALIFICATION_NOT_FOUND_MESSAGE_EXCEPTION = "Qualification not found";

    public static final String PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION = "Practitioner not found";
    public static final String SPECIALTY_NOT_FOUND_MESSAGE_EXCEPTION = "Specialty not found";

    public static final String DUPLICATE_IDENTIFIER_MESSAGE_EXCEPTION = "Identifier already exists";

    public static final Integer DEFAULT_PAGE_SIZE = 5;

}

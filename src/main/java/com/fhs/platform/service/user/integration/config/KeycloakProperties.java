package com.fhs.platform.service.user.integration.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "keycloak")
@Component
@Getter
@Setter
public class KeycloakProperties {

    private String realm;
    private String authServerUrl;
    private String clientId;
    private String clientSecret;
    private String adminUsername;
    private String adminPassword;

}

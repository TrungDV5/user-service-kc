package com.fhs.platform.service.user.controller.request.update;

import java.util.Set;
import lombok.Data;

@Data

public class PractitionerUpdateDto {

    private String familyName;
    private String givenName;
    private String prefixName;
    private String suffixName;
    private String gender;
    private String birthDate;
    private String email;
    private String experience;
    private String specialty;
    private Set<QualificationUpdateDto> qualifications;
    private Set<AddressUpdateDto> addresses;
    private Set<IdentifierUpdateDto> identifiers;

}

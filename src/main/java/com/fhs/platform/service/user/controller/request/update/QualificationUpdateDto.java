package com.fhs.platform.service.user.controller.request.update;

import lombok.Data;

@Data
public class QualificationUpdateDto {

    private String code;
    private String name;
    private String startValid;
    private String endValid;
    private String issuer;

}

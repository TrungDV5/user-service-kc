--drop schema if exists user_service_schema cascade;

SET search_path to user_service_schema;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;


CREATE TABLE if not exists patient (
	id uuid PRIMARY KEY,
	email varchar(255) NULL,
	phone_number varchar(255) not null,
	active BOOL default 't' not null,
	family_name varchar(255) not null,
	given_name varchar(255) not null,
	prefix_name varchar(255) not null,
	suffix_name varchar(255) null,
	gender CHAR not null,
	birth_date DATE not null,
	deceased BOOL default 'f' not null,
	deceased_date_time TIMESTAMP null,
	marital_status varchar(255) not null,
	managing_organization varchar(255) not null,
	role varchar(255) not null,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    is_deleted BOOL default 'f' not null
);

CREATE TABLE if not exists  practitioner (
	id uuid PRIMARY KEY,
	active BOOL default 't' not null,
	family_name varchar(255) not null,
	given_name varchar(255) not null,
	prefix_name varchar(255) not null,
	suffix_name varchar(255) not null,
	email varchar(255) not null,
	gender CHAR not null,
	birth_date DATE not null,
	role varchar(255) not null,
	experience integer not null,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    specialty varchar(255) not null,
    is_deleted BOOL default 'f' not null
);

create table if not exists  patient_practitioner(
	practitioner_id uuid not null,
	patient_id uuid not null,
	primary key(practitioner_id, patient_id),
	constraint fk_practitioner foreign key(practitioner_id) references practitioner(id),
	constraint fk_patient foreign key(patient_id) references patient(id)
);

create table if not exists  qualification(
	id uuid PRIMARY KEY,
	name varchar(255) not null,
	code varchar(255) not null,
	start_valid date not null,
	end_valid date not null,
	issuer varchar(255) not null,
	practitioner_id uuid not null,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    is_deleted BOOL default 'f' not null,
	constraint fk_qualification_practitioner foreign key(practitioner_id) references practitioner(id)
);

create table if not exists address(
    id uuid  PRIMARY KEY,
    use varchar(255) not null,
    type varchar(255) not null,
    text varchar(255) not null,
    line varchar(255) not null,
    city varchar(255) not null,
    district varchar(255) not null,
    state varchar(255) not null,
    postal_code varchar(255) not null,
    country varchar(255) not null,
    patient_id uuid,
    practitioner_id uuid,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    is_deleted BOOL default 'f' not null,
    constraint fk_address_patient foreign key(patient_id) references patient(id),
    constraint fk_address_practitioner foreign key(practitioner_id) references practitioner(id)
);



create table if not exists contact(
    id uuid PRIMARY KEY,
	name varchar(255) not null,
	gender char not null,
	email varchar(255) null,
	phone_number varchar(255) not null,
	organization varchar(255) not null,
	relationship varchar(255) not null,
	address_id uuid not null,
	patient_id uuid not null,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    is_deleted BOOL default 'f' not null,
	constraint fk_contact_address foreign key(address_id) references address(id),
	constraint fk_contact_patient foreign key(patient_id) references patient(id)
);


create table if not exists identifier(
    id uuid PRIMARY KEY,
    value varchar(255) not null,
    identifier_type varchar(255) not null,
    start_valid date null,
    end_valid date null,
    assigner varchar(255) not null,
    patient_id uuid null,
    practitioner_id uuid null,
    created_by uuid not null,
    created_on TIMESTAMP default CURRENT_TIMESTAMP not null,
    updated_by uuid not null,
    updated_on TIMESTAMP not null,
    is_deleted BOOL default 'f' not null,
    unique(identifier_type, value),
    constraint fk_identifier_patient foreign key(patient_id) references patient(id),
    constraint fk_identifier_practitioner foreign key(practitioner_id) references practitioner(id)
);
